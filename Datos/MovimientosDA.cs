﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Transactions;
using System.Globalization;

namespace Datos
{
    public class MovimientosDA
    {
        private string ConexionOrigen = ConfigurationManager.AppSettings.Get("ConexionBaseOrigen").ToString();
        private string ConexionDestino = ConfigurationManager.AppSettings.Get("ConexionBaseDestino").ToString();
       
        public string TraerInterno()
        {
            SqlConnection oConn = new SqlConnection(ConexionDestino);
            SqlCommand oComm = new SqlCommand();
            string proximo = "";
            try
            {
                using (oConn)
                {
                    oConn.Open();
                    oComm.Connection = oConn;

                    oComm.CommandText = "SELECT isnull(CONVERT(VarChar, CAST(MAX(NCOMP_IN_S) AS INT) + 1),1) AS PROXIMO FROM STA14 WHERE TCOMP_IN_S = '" + ConfigurationManager.AppSettings.Get("TCOMP_IN_S").ToString() + "'";
                    oComm.Parameters.Clear();
                    oComm.ExecuteNonQuery();
                    SqlDataReader reader = oComm.ExecuteReader();
                    if (reader.Read())
                    {
                        proximo = reader["PROXIMO"].ToString();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return proximo;
        }

        public string TraerNumeroComprobante()
        {
            SqlConnection oConn = new SqlConnection(ConexionDestino);
            SqlCommand oComm = new SqlCommand();
            string numeroVenta = "";
            string sucursal = "";
            try
            {
                using (oConn)
                {
                    oConn.Open();
                    oComm.Connection = oConn;

                    oComm.CommandText = "SELECT PROXIMO, SUCURSAL FROM STA17 WHERE TALONARIO ='" + ConfigurationManager.AppSettings.Get("TALONARIO").ToString() + "';";
                    oComm.Parameters.Clear();
                    oComm.ExecuteNonQuery();
                    SqlDataReader reader = oComm.ExecuteReader();
                    if (reader.Read())
                    {
                        numeroVenta = reader["PROXIMO"].ToString();
                        sucursal = reader["SUCURSAL"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            numeroVenta = numeroVenta.PadLeft(8, '0');
            sucursal = sucursal.PadLeft(5, '0');
            string N_COMP = " " + sucursal + numeroVenta;
            return N_COMP;
        }

		public int TraerProximoSTA17()
		{
			SqlConnection oConn = new SqlConnection(ConexionDestino);
			SqlCommand oComm = new SqlCommand();
			int proximo = 0;
			try
			{
				using (oConn)
				{
					oConn.Open();
					oComm.Connection = oConn;

					oComm.CommandText = "SELECT isnull(CONVERT(VarChar, CAST(MAX(PROXIMO) AS INT)),1) AS PROXIMO FROM STA17 WHERE TALONARIO =" + ConfigurationManager.AppSettings.Get("Talonario").ToString() + ";";
					oComm.Parameters.Clear();
					SqlDataReader reader = oComm.ExecuteReader();
					if (reader.Read())
					{
						proximo = Convert.ToInt32(reader["PROXIMO"].ToString());
					}
					oComm.Parameters.Clear();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return proximo;
		}

		public bool InsertarSiguienteSTA17()
		{
			SqlConnection oConn = new SqlConnection(ConexionDestino);
			SqlCommand oComm = new SqlCommand();
			bool bGuardo;
			try
			{
				using (oConn)
				{
					oConn.Open();
					oComm.Connection = oConn;
					int ProximoAinsertar = TraerProximoSTA17() + 1;

					oComm.CommandText = @"UPDATE [STA17] SET PROXIMO=@PROXIMO WHERE TALONARIO='" + ConfigurationManager.AppSettings.Get("Talonario").ToString() + "'";

					oComm.Parameters.AddWithValue("@PROXIMO", ProximoAinsertar);
					oComm.ExecuteNonQuery();
				}
				bGuardo = true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return bGuardo;
		}

		public void RecomponerSaldos()
		{
			SqlConnection oConn = new SqlConnection(ConexionDestino);
			SqlCommand oComm = new SqlCommand();
			try
			{
				using (oConn)
				{
					oConn.Open();
					oComm.Connection = oConn;
					oComm.CommandText =
						   @"
                             exec sp_RecomposicionSaldosStock
                            ";
					oComm.Parameters.Clear();
                    oComm.CommandTimeout = 120;
                    oComm.ExecuteNonQuery();
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool InsertoTablaPropia(Entidades.STA14EncabezadoStock sta14encabStock, string Interno, string n_comp)
        {
            SqlConnection oConn = new SqlConnection(ConexionOrigen);
            SqlCommand oComm = new SqlCommand();
            bool bGuardo;
            
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (oConn)
                    {
                        oConn.Open();
                        oComm.Connection = oConn;


                        oComm.CommandText =
							@"
								INSERT INTO SeinMovimientosPasados 
								(
									FECHA_GRABADO,
									NCOMP_ORIG,
									TCOMP_ORIG,
									NCOMP_INT_ORIG,
									TCOMP_INT_ORIG,
									NCOMP_DEST,
									TCOMP_DEST,
									NCOMP_INT_DEST,
									TCOMP_INT_DEST	
								)
								VALUES 
								(
									@FECHA_GRABADO,
									@NCOMP_ORIG,
									@TCOMP_ORIG,
									@NCOMP_INT_ORIG,
									@TCOMP_INT_ORIG,
									@NCOMP_DEST,
									@TCOMP_DEST,
									@NCOMP_INT_DEST,
									@TCOMP_INT_DEST
								)
							";

                        oComm.Parameters.Clear();
                        oComm.Parameters.Add("@FECHA_GRABADO", SqlDbType.DateTime).Value = sta14encabStock.FECHA_MOV;
                        oComm.Parameters.Add("@NCOMP_ORIG", SqlDbType.VarChar).Value = sta14encabStock.N_COMP;
                        oComm.Parameters.Add("@TCOMP_ORIG", SqlDbType.VarChar).Value = sta14encabStock.T_COMP;
                        oComm.Parameters.Add("@NCOMP_INT_ORIG", SqlDbType.VarChar).Value = sta14encabStock.NCOMP_IN_S;
                        oComm.Parameters.Add("@TCOMP_INT_ORIG", SqlDbType.VarChar).Value = sta14encabStock.TCOMP_IN_S;
                        oComm.Parameters.Add("@NCOMP_DEST", SqlDbType.VarChar).Value = n_comp;
                        oComm.Parameters.Add("@TCOMP_DEST", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings.Get("TCOMP").ToString();
                        oComm.Parameters.Add("@NCOMP_INT_DEST", SqlDbType.VarChar).Value = Interno;
                        oComm.Parameters.Add("@TCOMP_INT_DEST", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings.Get("TCOMP_IN_S").ToString();
                        oComm.ExecuteNonQuery();

                    }
                    scope.Complete();
                    bGuardo = true;
                    return bGuardo;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
		
        public List<Entidades.STA14EncabezadoStock> TraerMovimientosEncabezadoStockBase2()
        {
            SqlConnection oConn = new SqlConnection(ConexionOrigen);
            SqlCommand oComm = new SqlCommand();
            List<Entidades.STA14EncabezadoStock> listEncabStock = new List<Entidades.STA14EncabezadoStock>();
            try
            {
                using (oConn)
                {
                    oConn.Open();
                    oComm.Connection = oConn;

                    oComm.CommandText =
                            @"
								select 
									t2.N_COMP,
									t2.T_COMP,
									t2.NCOMP_IN_S, 
									t2.TCOMP_IN_S,
									t2.FECHA_MOV
								FROM STA14 t2
								where 
									t2.FECHA_ANU = '18000101' AND
									t2.T_COMP = 'FAC' AND
									NOT EXISTS 
									(
										select 
											* 
										from SeinMovimientosPasados t1 
										where 
											t1.NCOMP_ORIG COLLATE Latin1_General_BIN = t2.N_COMP and 
											t1.TCOMP_ORIG COLLATE Latin1_General_BIN = t2.t_COMP and 
											t1.NCOMP_INT_ORIG COLLATE Latin1_General_BIN = t2.ncomp_in_s and 
											t1.TCOMP_INT_ORIG COLLATE Latin1_General_BIN = t2.tcomp_in_s
									) AND 
									t2.FECHA_MOV >= '20210914'
                            ";
                    oComm.ExecuteNonQuery();
                    SqlDataReader reader = oComm.ExecuteReader();
                    while (reader.Read())
                    {
                        Entidades.STA14EncabezadoStock oEncabezadoStock = new Entidades.STA14EncabezadoStock();
                        oEncabezadoStock.N_COMP = reader["N_COMP"].ToString();
                        oEncabezadoStock.T_COMP = reader["T_COMP"].ToString();
                        oEncabezadoStock.NCOMP_IN_S = reader["NCOMP_IN_S"].ToString();
                        oEncabezadoStock.TCOMP_IN_S = reader["TCOMP_IN_S"].ToString();
                        oEncabezadoStock.FECHA_MOV = Convert.ToDateTime(reader["FECHA_MOV"]);
                        listEncabStock.Add(oEncabezadoStock);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listEncabStock;
        }

        public bool ModificaStock(Entidades.STA14EncabezadoStock sTA14EncabStock, string Interno, string n_comp)
        {
            SqlConnection oConn = new SqlConnection(ConexionDestino);
            SqlCommand oComm = new SqlCommand();
            bool bGuardo;

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (oConn)
                    {
                        oConn.Open();
                        oComm.Connection = oConn;

                        

                        oComm.CommandText = "INSERT INTO [STA14] " + "(FILLER,COD_PRO_CL,COTIZ,ESTADO_MOV,EXPORTADO,FECHA_MOV,LISTA_REM,LOTE,MON_CTE,MOTIVO_REM,NRO_SUCURS,N_COMP,NCOMP_IN_S,N_REMITO,OBSERVACIO,TALONARIO,T_COMP,TCOMP_IN_S,FECHA_ANU,HORA,LOTE_ANU,NCOMP_ORIG,SUC_ORIG,TCOMP_ORIG,USUARIO,EXP_STOCK,COD_TRANSP,HORA_COMP,ID_A_RENTA,DOC_ELECTR,COD_CLASIF,AUDIT_IMP,IMP_IVA,IMP_OTIMP,IMPORTE_BO,IMPORTE_TO,DIFERENCIA,SUC_DESTIN,T_DOC_DTE,LEYENDA1,LEYENDA2,LEYENDA3,LEYENDA4,LEYENDA5,DCTO_CLIEN,T_INT_ORI,N_INT_ORI,FECHA_INGRESO,HORA_INGRESO,USUARIO_INGRESO,TERMINAL_INGRESO,IMPORTE_TOTAL_CON_IMPUESTOS,CANTIDAD_KILOS,ID_DIRECCION_ENTREGA,IMPORTE_GRAVADO,IMPORTE_EXENTO)" + " VALUES " + "(@FILLER,@COD_PRO_CL,@COTIZ,@ESTADO_MOV,@EXPORTADO,@FECHA_MOV,@LISTA_REM,@LOTE,@MON_CTE,@MOTIVO_REM,@NRO_SUCURS,@N_COMP,@NCOMP_IN_S,@N_REMITO,@OBSERVACIO,@TALONARIO,@T_COMP,@TCOMP_IN_S,@FECHA_ANU,@HORA,@LOTE_ANU,@NCOMP_ORIG,@SUC_ORIG,@TCOMP_ORIG,@USUARIO,@EXP_STOCK,@COD_TRANSP,@HORA_COMP,@ID_A_RENTA,@DOC_ELECTR,@COD_CLASIF,@AUDIT_IMP,@IMP_IVA,@IMP_OTIMP,@IMPORTE_BO,@IMPORTE_TO,@DIFERENCIA,@SUC_DESTIN,@T_DOC_DTE,@LEYENDA1,@LEYENDA2,@LEYENDA3,@LEYENDA4,@LEYENDA5,@DCTO_CLIEN,@T_INT_ORI,@N_INT_ORI,@FECHA_INGRESO,@HORA_INGRESO,@USUARIO_INGRESO,@TERMINAL_INGRESO,@IMPORTE_TOTAL_CON_IMPUESTOS,@CANTIDAD_KILOS,@ID_DIRECCION_ENTREGA,@IMPORTE_GRAVADO,@IMPORTE_EXENTO)";

                        oComm.Parameters.Clear();
                        oComm.Parameters.Add("@FILLER", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@COD_PRO_CL", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@COTIZ", SqlDbType.Decimal).Value = 1;
                        oComm.Parameters.Add("@ESTADO_MOV", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@EXPORTADO", SqlDbType.Bit).Value = 0;
						oComm.Parameters.Add("@FECHA_MOV", SqlDbType.DateTime).Value = sTA14EncabStock.FECHA_MOV;
                        oComm.Parameters.Add("@LISTA_REM", SqlDbType.Int).Value = 0;
                        oComm.Parameters.Add("@LOTE", SqlDbType.Decimal).Value = 0;
                        oComm.Parameters.Add("@MON_CTE", SqlDbType.Bit).Value = 1;
                        oComm.Parameters.Add("@MOTIVO_REM", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@NRO_SUCURS", SqlDbType.Int).Value = 0;
                        oComm.Parameters.Add("@N_COMP", SqlDbType.VarChar).Value = n_comp;
                        oComm.Parameters.Add("@NCOMP_IN_S", SqlDbType.VarChar).Value = Interno;
                        oComm.Parameters.Add("@N_REMITO", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@OBSERVACIO", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@TALONARIO", SqlDbType.Int).Value = ConfigurationManager.AppSettings.Get("TALONARIO").ToString();
                        oComm.Parameters.Add("@T_COMP", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings.Get("TCOMP").ToString();
                        oComm.Parameters.Add("@TCOMP_IN_S", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings.Get("TCOMP_IN_S").ToString();
                        oComm.Parameters.Add("@FECHA_ANU", SqlDbType.DateTime).Value = DateTime.ParseExact("18000101", "yyyyMMdd", CultureInfo.InvariantCulture).Date;
                        oComm.Parameters.Add("@HORA", SqlDbType.VarChar).Value = "0000";
                        oComm.Parameters.Add("@LOTE_ANU", SqlDbType.Decimal).Value = 0;
                        oComm.Parameters.Add("@NCOMP_ORIG", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@SUC_ORIG", SqlDbType.Int).Value = 0;
                        oComm.Parameters.Add("@TCOMP_ORIG", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@USUARIO", SqlDbType.VarChar).Value = "Seincomp";
                        oComm.Parameters.Add("@EXP_STOCK", SqlDbType.Bit).Value = 0;
                        oComm.Parameters.Add("@COD_TRANSP", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@HORA_COMP", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@ID_A_RENTA", SqlDbType.Decimal).Value = 0;
                        oComm.Parameters.Add("@DOC_ELECTR", SqlDbType.Bit).Value = 0;
                        oComm.Parameters.Add("@COD_CLASIF", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@AUDIT_IMP", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@IMP_IVA", SqlDbType.Decimal).Value = 0;
                        oComm.Parameters.Add("@IMP_OTIMP", SqlDbType.Decimal).Value = 0;
                        oComm.Parameters.Add("@IMPORTE_BO", SqlDbType.Decimal).Value = 0;
                        oComm.Parameters.Add("@IMPORTE_TO", SqlDbType.Decimal).Value = 0;
                        oComm.Parameters.Add("@DIFERENCIA", SqlDbType.VarChar).Value = "N";
                        oComm.Parameters.Add("@SUC_DESTIN", SqlDbType.Int).Value = 0;
                        oComm.Parameters.Add("@T_DOC_DTE", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@LEYENDA1", SqlDbType.VarChar).Value = sTA14EncabStock.N_COMP;
                        oComm.Parameters.Add("@LEYENDA2", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@LEYENDA3", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@LEYENDA4", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@LEYENDA5", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@DCTO_CLIEN", SqlDbType.Decimal).Value = 0;
                        oComm.Parameters.Add("@T_INT_ORI", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@N_INT_ORI", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@FECHA_INGRESO", SqlDbType.DateTime).Value = DateTime.ParseExact("18000101", "yyyyMMdd", CultureInfo.InvariantCulture).Date;
                        oComm.Parameters.Add("@HORA_INGRESO", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@USUARIO_INGRESO", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@TERMINAL_INGRESO", SqlDbType.VarChar).Value = "";
                        oComm.Parameters.Add("@IMPORTE_TOTAL_CON_IMPUESTOS", SqlDbType.Decimal).Value = 0;
                        oComm.Parameters.Add("@CANTIDAD_KILOS", SqlDbType.Decimal).Value = 0;
                        oComm.Parameters.Add("@ID_DIRECCION_ENTREGA", SqlDbType.Int).Value = DBNull.Value;
                        oComm.Parameters.Add("@IMPORTE_GRAVADO", SqlDbType.Decimal).Value = 0;
                        oComm.Parameters.Add("@IMPORTE_EXENTO", SqlDbType.Decimal).Value = 0;
                        oComm.ExecuteNonQuery();


                        SqlDataAdapter da = null;
                        DataTable dt = new DataTable();
                        string consulta = string.Empty;
                        List<Entidades.STA20DetalleStock> listDetallStock = new List<Entidades.STA20DetalleStock>();

                        oComm.Parameters.Clear();
                        using (SqlConnection oConn2 = new SqlConnection(ConexionOrigen))
                        {
							

							da = new SqlDataAdapter(@"
										SELECT 
											COD_ARTICU, 
											CANTIDAD, 
											COD_DEPOSI, 
											ISNULL(ID_MEDIDA_STOCK_2, -1) AS ID_MEDIDA_STOCK_2, 
											ISNULL(ID_MEDIDA_STOCK, -1) AS ID_MEDIDA_STOCK, 
											ISNULL(ID_MEDIDA_VENTAS, -1) AS ID_MEDIDA_VENTAS, 
											ISNULL(ID_MEDIDA_COMPRA, -1) AS ID_MEDIDA_COMPRA,
											UNIDAD_MEDIDA_SELECCIONADA
										FROM STA20 WHERE TCOMP_IN_S = '" + sTA14EncabStock.TCOMP_IN_S + "' AND NCOMP_IN_S='" + sTA14EncabStock.NCOMP_IN_S + "';", oConn2);
                            da.Fill(dt);
                        } 
                           
                        foreach (DataRow row in dt.Rows)
                        {
                            Entidades.STA20DetalleStock oDetalleStock = new Entidades.STA20DetalleStock();
                            oDetalleStock.COD_ARTICU = row["COD_ARTICU"].ToString();
                            oDetalleStock.CANTIDAD = Convert.ToDouble(row["CANTIDAD"]);
                            oDetalleStock.COD_DEPOSI = row["COD_DEPOSI"].ToString();
							oDetalleStock.ID_MEDIDA_STOCK_2 = Convert.ToInt32(row["ID_MEDIDA_STOCK_2"]);
							oDetalleStock.ID_MEDIDA_STOCK = Convert.ToInt32(row["ID_MEDIDA_STOCK"]);
							oDetalleStock.ID_MEDIDA_VENTAS = Convert.ToInt32(row["ID_MEDIDA_VENTAS"]);
							oDetalleStock.ID_MEDIDA_COMPRA = Convert.ToInt32(row["ID_MEDIDA_COMPRA"]);
							oDetalleStock.UNIDAD_MEDIDA_SELECCIONADA = row["UNIDAD_MEDIDA_SELECCIONADA"].ToString();

							listDetallStock.Add(oDetalleStock);
                        }

                        int renglon = 0;
                        foreach (Entidades.STA20DetalleStock item in listDetallStock)
                        {
                          
                                renglon++;
                                oComm.CommandText = "INSERT INTO [STA20] " + "(FILLER,CANTIDAD,CANT_DEV,CANT_OC,CANT_PEND,CANT_SCRAP,CAN_EQUI_V,COD_ARTICU,COD_DEPOSI,DEPOSI_DDE,EQUIVALENC,FECHA_MOV,NCOMP_IN_S,N_ORDEN_CO,N_RENGL_OC,N_RENGL_S,PLISTA_REM,PPP_EX,PPP_LO,PRECIO,PRECIO_REM,TIPO_MOV,TCOMP_IN_S,COD_CLASIF,CANT_FACTU,DCTO_FACTU,CANT_DEV_2,CANT_PEND_2,CANTIDAD_2,CANT_FACTU_2,ID_MEDIDA_STOCK_2,ID_MEDIDA_STOCK,ID_MEDIDA_VENTAS,ID_MEDIDA_COMPRA,UNIDAD_MEDIDA_SELECCIONADA,PRECIO_REMITO_VENTAS,CANT_OC_2,RENGL_PADR,COD_ARTICU_KIT,PROMOCION,PRECIO_ADICIONAL_KIT,TALONARIO_OC)" + " VALUES " + "(@FILLER,@CANTIDAD,@CANT_DEV,@CANT_OC,@CANT_PEND,@CANT_SCRAP,@CAN_EQUI_V,@COD_ARTICU,@COD_DEPOSI,@DEPOSI_DDE,@EQUIVALENC,@FECHA_MOV,@NCOMP_IN_S,@N_ORDEN_CO,@N_RENGL_OC,@N_RENGL_S,@PLISTA_REM,@PPP_EX,@PPP_LO,@PRECIO,@PRECIO_REM,@TIPO_MOV,@TCOMP_IN_S,@COD_CLASIF,@CANT_FACTU,@DCTO_FACTU,@CANT_DEV_2,@CANT_PEND_2,@CANTIDAD_2,@CANT_FACTU_2,@ID_MEDIDA_STOCK_2,@ID_MEDIDA_STOCK,@ID_MEDIDA_VENTAS,@ID_MEDIDA_COMPRA,@UNIDAD_MEDIDA_SELECCIONADA,@PRECIO_REMITO_VENTAS,@CANT_OC_2,@RENGL_PADR,@COD_ARTICU_KIT,@PROMOCION,@PRECIO_ADICIONAL_KIT,@TALONARIO_OC)";

                                oComm.Parameters.Clear();
                                oComm.Parameters.Add("@FILLER", SqlDbType.VarChar).Value = "Seincomp";
                                oComm.Parameters.Add("@CANTIDAD", SqlDbType.Decimal).Value = item.CANTIDAD;
                                oComm.Parameters.Add("@CANT_DEV", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@CANT_OC", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@CANT_PEND", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@CANT_SCRAP", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@CAN_EQUI_V", SqlDbType.Decimal).Value = 1;
                                oComm.Parameters.Add("@COD_ARTICU", SqlDbType.VarChar).Value = item.COD_ARTICU;
                                oComm.Parameters.Add("@COD_DEPOSI", SqlDbType.VarChar).Value = item.COD_DEPOSI;
                                oComm.Parameters.Add("@DEPOSI_DDE", SqlDbType.VarChar).Value = "";
                                oComm.Parameters.Add("@EQUIVALENC", SqlDbType.Decimal).Value = 1;
                                oComm.Parameters.Add("@FECHA_MOV", SqlDbType.DateTime).Value = sTA14EncabStock.FECHA_MOV;
                                oComm.Parameters.Add("@NCOMP_IN_S", SqlDbType.VarChar).Value = Interno;
                                oComm.Parameters.Add("@N_ORDEN_CO", SqlDbType.VarChar).Value = "";
                                oComm.Parameters.Add("@N_RENGL_OC", SqlDbType.Int).Value = 0;
                                oComm.Parameters.Add("@N_RENGL_S", SqlDbType.Int).Value = renglon;
                                oComm.Parameters.Add("@PLISTA_REM", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@PPP_EX", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@PPP_LO", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@PRECIO", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@PRECIO_REM", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@TIPO_MOV", SqlDbType.VarChar).Value = "S";
                                oComm.Parameters.Add("@TCOMP_IN_S", SqlDbType.VarChar).Value = ConfigurationManager.AppSettings.Get("TCOMP_IN_S").ToString(); ;
                                oComm.Parameters.Add("@COD_CLASIF", SqlDbType.VarChar).Value = "";
                                oComm.Parameters.Add("@CANT_FACTU", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@DCTO_FACTU", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@CANT_DEV_2", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@CANT_PEND_2", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@CANTIDAD_2", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@CANT_FACTU_2", SqlDbType.Decimal).Value = 0;

								if (item.ID_MEDIDA_STOCK_2 == -1)
									oComm.Parameters.Add("@ID_MEDIDA_STOCK_2", SqlDbType.Int).Value = DBNull.Value;
								else
									oComm.Parameters.Add("@ID_MEDIDA_STOCK_2", SqlDbType.Int).Value = item.ID_MEDIDA_STOCK_2;

								if (item.ID_MEDIDA_STOCK == -1)
									oComm.Parameters.Add("@ID_MEDIDA_STOCK", SqlDbType.Int).Value = DBNull.Value;
								else
									oComm.Parameters.Add("@ID_MEDIDA_STOCK", SqlDbType.Int).Value = item.ID_MEDIDA_STOCK;

								if (item.ID_MEDIDA_VENTAS == -1)
									oComm.Parameters.Add("@ID_MEDIDA_VENTAS", SqlDbType.Int).Value = DBNull.Value;
								else
									oComm.Parameters.Add("@ID_MEDIDA_VENTAS", SqlDbType.Int).Value = item.ID_MEDIDA_VENTAS;

								if (item.ID_MEDIDA_COMPRA == -1)
									oComm.Parameters.Add("@ID_MEDIDA_COMPRA", SqlDbType.Int).Value = DBNull.Value;
								else
									oComm.Parameters.Add("@ID_MEDIDA_COMPRA", SqlDbType.Int).Value = item.ID_MEDIDA_COMPRA;

                                oComm.Parameters.Add("@UNIDAD_MEDIDA_SELECCIONADA", SqlDbType.VarChar).Value = item.UNIDAD_MEDIDA_SELECCIONADA;
                                oComm.Parameters.Add("@PRECIO_REMITO_VENTAS", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@CANT_OC_2", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@RENGL_PADR", SqlDbType.Int).Value = 0;
                                oComm.Parameters.Add("@COD_ARTICU_KIT", SqlDbType.VarChar).Value = "";
                                oComm.Parameters.Add("@PROMOCION", SqlDbType.Bit).Value = 0;
                                oComm.Parameters.Add("@PRECIO_ADICIONAL_KIT", SqlDbType.Decimal).Value = 0;
                                oComm.Parameters.Add("@TALONARIO_OC", SqlDbType.Int).Value = 0;
                                oComm.ExecuteNonQuery();
                            }
                        }
                        scope.Complete();
                        bGuardo = true;
                        return bGuardo;
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
		
    }
}
