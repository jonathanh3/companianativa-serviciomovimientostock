﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class STA14EncabezadoStock
    {

        public int ID_STA14 { get; set; }

        public string FILLER { get; set; }

        public string COD_PRO_CL { get; set; }

        public double COTIZ { get; set; }

        public string ESTADO_MOV { get; set; }

        public bool EXPORTADO { get; set; }

        public bool EXP_STOCK { get; set; }

        public DateTime FECHA_ANU { get; set; }

        public DateTime FECHA_MOV { get; set; }

        public string HORA { get; set; }

        public int LISTA_REM { get; set; }

        public double LOTE { get; set; }

        public double LOTE_ANU { get; set; }

        public bool MON_CTE { get; set; }

        public string MOTIVO_REM { get; set; }

        public string N_COMP { get; set; }

        public string N_REMITO { get; set; }

        public string NCOMP_IN_S { get; set; }

        public string NCOMP_ORIG { get; set; }

        public int NRO_SUCURS { get; set; }

        public string OBSERVACIO { get; set; }

        public int SUC_ORIG { get; set; }

        public string T_COMP { get; set; }

        public int TALONARIO { get; set; }

        public string TCOMP_IN_S { get; set; }

        public string TCOMP_ORIG { get; set; }

        public string USUARIO { get; set; }

        public string COD_TRANSP { get; set; }

        public string HORA_COMP { get; set; }

        public double ID_A_RENTA { get; set; }

        public bool DOC_ELECTR { get; set; }

        public string COD_CLASIF { get; set; }

        public string AUDIT_IMP { get; set; }

        public double IMP_IVA { get; set; }

        public double IMP_OTIMP { get; set; }

        public double IMPORTE_BO { get; set; }

        public double IMPORTE_TO { get; set; }

        public string DIFERENCIA { get; set; }

        public int SUC_DESTIN { get; set; }

        public string T_DOC_DTE { get; set; }

        public string LEYENDA1 { get; set; }

        public string LEYENDA2 { get; set; }

        public string LEYENDA3 { get; set; }

        public string LEYENDA4 { get; set; }

        public string LEYENDA5 { get; set; }

        public double DCTO_CLIEN { get; set; }

        public string T_INT_ORI { get; set; }

        public string N_INT_ORI { get; set; }

        public DateTime FECHA_INGRESO { get; set; }

        public string HORA_INGRESO { get; set; }

        public string USUARIO_INGRESO { get; set; }

        public string TERMINAL_INGRESO { get; set; }

        public double IMPORTE_TOTAL_CON_IMPUESTOS { get; set; }

        public double CANTIDAD_KILOS { get; set; }

        public int ID_DIRECCION_ENTREGA { get; set; }

        public double IMPORTE_GRAVADO { get; set; }

        public double IMPORTE_EXENTO { get; set; }
        public List<Entidades.STA20DetalleStock> Detalle { get; set; }
        public STA14EncabezadoStock()
        {
            this.Detalle = new List<STA20DetalleStock>();
        }
    }
}
