﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class STA20DetalleStock
    {
        public int ID_STA20 { get; set; }

        public string FILLER { get; set; }

        public double CAN_EQUI_V { get; set; }

        public double CANT_DEV { get; set; }

        public double CANT_OC { get; set; }

        public double CANT_PEND { get; set; }

        public double CANT_SCRAP { get; set; }

        public double CANTIDAD { get; set; }

        public string COD_ARTICU { get; set; }

        public string COD_DEPOSI { get; set; }

        public string DEPOSI_DDE { get; set; }

        public double EQUIVALENC { get; set; }

        public DateTime FECHA_MOV { get; set; }

        public string N_ORDEN_CO { get; set; }

        public int N_RENGL_OC { get; set; }

        public int N_RENGL_S { get; set; }

        public string NCOMP_IN_S { get; set; }

        public double PLISTA_REM { get; set; }

        public double PPP_EX { get; set; }

        public double PPP_LO { get; set; }

        public double PRECIO { get; set; }

        public double PRECIO_REM { get; set; }

        public string TCOMP_IN_S { get; set; }

        public string TIPO_MOV { get; set; }

        public string COD_CLASIF { get; set; }

        public double CANT_FACTU { get; set; }

        public double DCTO_FACTU { get; set; }

        public double CANT_DEV_2 { get; set; }

        public double CANT_PEND_2 { get; set; }

        public double CANTIDAD_2 { get; set; }

        public double CANT_FACTU_2 { get; set; }

        public int ID_MEDIDA_STOCK_2 { get; set; }

        public int ID_MEDIDA_STOCK { get; set; }

        public int ID_MEDIDA_VENTAS { get; set; }

        public int ID_MEDIDA_COMPRA { get; set; }

        public string UNIDAD_MEDIDA_SELECCIONADA { get; set; }

        public double PRECIO_REMITO_VENTAS { get; set; }

        public double CANT_OC_2 { get; set; }

        public int RENGL_PADR { get; set; }

        public string COD_ARTICU_KIT { get; set; }

        public bool PROMOCION { get; set; }

        public double PRECIO_ADICIONAL_KIT { get; set; }

        public int TALONARIO_OC { get; set; }
        
    }
}
