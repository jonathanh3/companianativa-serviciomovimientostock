﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class SeinMovimientosPasados
    {
        public int ID_MOVIMIENTO { get; set; }
        public DateTime FECHA_GRABADO { get; set; }
        public string NCOMP_ORIG { get; set; }
        public string TCOMP_ORIG { get; set; }
        public string NCOMP_INT_ORIG { get; set; }
        public string TCOMP_INT_ORIG { get; set; }
        public string NCOMP_DEST { get; set; }
        public string TCOMP_DEST { get; set; }
        public string NCOMP_INT_DEST { get; set; }
        public string TCOMP_INT_DEST { get; set; }

    }
}
