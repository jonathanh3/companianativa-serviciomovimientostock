﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;


namespace LogErrores
{
    public class Global
    {
        public void LogErrores(Exception ex)
        {
            using (System.IO.StreamWriter oLog = new System.IO.StreamWriter(Application.StartupPath + "\\Errores.log", true))
            {
                oLog.WriteLine(DateTime.Now.ToString("dd-MM-yyy HH:mm") + " - " + ex.Message);
            }
        }
    }
}
