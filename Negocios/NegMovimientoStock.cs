﻿using System;
using System.Collections.Generic;
using System.Transactions;

namespace Negocios
{
    public class NegMovimientoStock
    {
        public bool ModificarStock()
        {
            List<Entidades.STA14EncabezadoStock> listEncabStock = new List<Entidades.STA14EncabezadoStock>();
            List<Entidades.STA20DetalleStock> listDetalleStock = new List<Entidades.STA20DetalleStock>();

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    Datos.MovimientosDA movimientosDA = new Datos.MovimientosDA();
                    listEncabStock = movimientosDA.TraerMovimientosEncabezadoStockBase2();
                    if(listEncabStock.Count > 0)
                    {
                        foreach (var item in listEncabStock)
                        {
                            string Interno = movimientosDA.TraerInterno();
                            string n_comp = movimientosDA.TraerNumeroComprobante();
                            Interno = Interno.PadLeft(8, '0');

                            movimientosDA.ModificaStock(item, Interno, n_comp);
                            movimientosDA.InsertoTablaPropia(item, Interno, n_comp);
                            movimientosDA.InsertarSiguienteSTA17();
                        }
                        movimientosDA.RecomponerSaldos();
                        scope.Complete();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
