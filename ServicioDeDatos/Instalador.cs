﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace ServicioDeDatos
{
    [RunInstaller(true)]
    public partial class Instalador : System.Configuration.Install.Installer
    {
        private ServiceInstaller serviceInstaller1;
        private ServiceProcessInstaller processInstaller;
        public Instalador()
        {
            InitializeComponent();
            //Instantiate installers for process and services.

            processInstaller = new ServiceProcessInstaller();
            serviceInstaller1 = new ServiceInstaller();

            //The services will run under the system account.

            processInstaller.Account = ServiceAccount.LocalSystem;

            //The services will be started manually.

            serviceInstaller1.StartType = ServiceStartMode.Automatic;

            //ServiceName must equal those on ServiceBase derived classes.

            serviceInstaller1.ServiceName = "SeinInterfazMovimientoStock";

            //Add installers to collection. Order is not important.

            Installers.Add(serviceInstaller1);

            Installers.Add(processInstaller);
        }
    }
}
