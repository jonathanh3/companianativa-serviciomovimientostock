﻿using System;
using System.Configuration;
using System.IO;
using System.ServiceProcess;
using System.Timers;
using System.Windows.Forms;


namespace ServicioDeDatos
{
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer timer;
        public Service1()
        {
            InitializeComponent();
            timer = new System.Timers.Timer(Convert.ToInt32(ConfigurationManager.AppSettings.Get("MINUTOS").ToString()) * 60000);

            timer.Elapsed += new ElapsedEventHandler(Ejecutar);
        }
        private void Ejecutar(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            try
            {
                Negocios.NegMovimientoStock movimientoStock = new Negocios.NegMovimientoStock();
                movimientoStock.ModificarStock();
            }
            catch (Exception ex)
            {
                LogErrores(ex);
            }
            timer.Start();
        }
     
        protected override void OnStart(string[] args)
        {
            timer.Start();
        }

        public void LogErrores(Exception ex)
        {
            using (StreamWriter oLog = new StreamWriter(Application.StartupPath + "\\Errores.log", true))
            {
                oLog.WriteLine(DateTime.Now.ToString("dd-MM-yyy HH:mm") + " - " + ex.Message);
            }
        }

        protected override void OnStop()
        {
        }
    }
}
